<?php
include_once 'dbConfig.php';


class Product
{

    private $pdo;
    
    public function __construct()
    {
        $dataBase = new Database();
        $this->pdo = $dataBase->getConnection();   
    }

    public function getProducts()
    {
        $sql = "SELECT * FROM products";
        $smtp = $this->pdo->query($sql);
        $data = $smtp->fetchAll();

        return  $data;
    }

    public function createProduct($sku, $name, $price, $type_id, $size_mb, $weight, $heigth_cm, $width_cm, $length_cm)
    {
        $sql = "INSERT INTO products(sku, name, price, type_id, size_mb, weight, heigth_cm, width_cm, length_cm) 
        VALUES (:sku, :name, :price, :type_id, :size_mb, :weight, :heigth_cm, :width_cm, :length_cm)";
        $stmt = $this->pdo->prepare($sql);

        $stmt->bindParam(':sku', $sku);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':price', $price);
        $stmt->bindParam(':type_id', $type_id);
        $stmt->bindParam(':size_mb', $size_mb);
        $stmt->bindParam(':weight', $weight);
        $stmt->bindParam(':heigth_cm', $heigth_cm);
        $stmt->bindParam(':width_cm', $width_cm);
        $stmt->bindParam(':length_cm', $length_cm);

        $stmt->execute();
    }

    public function deleteProducts($ids) {
        $dataBase = new Database();
        $pdo = $dataBase->getConnection();

        $sql = "DELETE FROM products WHERE id IN (:ids)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':ids', $ids);
        $stmt->execute();
    }
}
