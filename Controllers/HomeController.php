<?php

include_once 'Models/Product.php';

class HomeController {
    public function homePage() {

        $product = new Product();
        $products = $product->getProducts();
        
        include_once 'views/home.php';
        return;
    }
}