<?php

class Database
{

    private $host = "localhost";
    private $db_name = "products_list";
    private $username = "root";
    private $password = "";
    public $DB_con;

    // get the database connection
    public function getConnection()
    {

        $this->DB_con = null;

        try {
            $this->DB_con = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->DB_con;
    }
}
