<?php
include_once 'Controllers/ProductController.php';
include_once 'Controllers/HomeController.php';


$page = 'home';
if (isset($_GET['page']) && $_GET['page'] != '') {
    $page = $_GET['page'];
}


switch ($page) {
    case  'add-product':
        $productController = new ProductController();

        $productController->addProductPage();

        break;

    case  'create-product':
        $productController = new ProductController();

        $productController->createProduct();

        break;

    case  'delete-products':
        $productController = new ProductController();

        $productController->deleteProducts();

        break;

    case 'home':
    default:
        $homeController = new HomeController();

        $homeController->homePage();

        break;
}


return;
